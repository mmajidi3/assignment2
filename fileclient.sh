#!/bin/bash

cd client

docker stop client || true
docker rm client || true
docker rmi client:1.0.0 || true

docker build -t client:1.0.0 .
docker run -v /clientdata:/data -e APP_HOST=192.168.164.138 -e APP_PORT=8080 --name client -it client:1.0.0


