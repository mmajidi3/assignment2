#!/bin/sh

echo -e "\n\nAPP_HOST is $APP_HOST"
echo -e "\n\nAPP_PORT is $APP_PORT"

ID=`curl 2>/dev/null  --location --request POST "http://$APP_HOST:$APP_PORT/api/persons" --header 'Content-Type: application/json'  -d '{  "name": "jack",    "family": "yarn"}'`

echo -e "\n\nnew Person added. ID is $ID"

echo -e "\n\nList of current users is: "

curl 2>/dev/null  --location --request GET "http://$APP_HOST:$APP_PORT/api/persons" 

echo -e "\n\nDetails of person with id $ID is:"

curl 2>/dev/null  --request GET "http://$APP_HOST:$APP_PORT/api/persons/$ID" 

echo -e "\n\nHTTP status of deleting user with id $ID is"

curl 2>/dev/null -s -o /dev/null -w "%{http_code}" --location --request DELETE "http://$APP_HOST:$APP_PORT/api/persons/$ID" 

echo -e "\n\nStatus code of creating a random 1kb text file at server is: "

curl 2>/dev/null -s -o /dev/null -w "%{http_code}" --location --request POST "http://$APP_HOST:$APP_PORT/api/create-file" --header 'Content-Type: application/json' -d '{    "name": "sample" }'

echo "" > /data/sample

curl  2>/dev/null  --location --request GET "http://$APP_HOST:$APP_PORT/api/download-file/sample"  -o /data/sample

echo -e "\n\nFile downloaded. Content of download file is:"

cat /data/sample

echo "" > /data/sample.sha512 

curl  2>/dev/null --request GET "http://$APP_HOST:$APP_PORT/api/download-file/sample.sha512"  -o /data/sample.sha512

echo -e "\n\nFile checksum downloaded. content is :"

cat /data/sample.sha512

echo -e "\n\nChecking checksum...\n"

echo -e "$(cat /data/sample.sha512) /data/sample" | sha512sum --check

echo -e "\n\n Done!\n"


echo -n "Press [ENTER] to continue,...: "
read var_name

