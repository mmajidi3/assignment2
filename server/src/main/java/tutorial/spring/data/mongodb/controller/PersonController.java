package tutorial.spring.data.mongodb.controller;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import com.google.common.hash.Hashing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import tutorial.spring.data.mongodb.model.FileDto;
import tutorial.spring.data.mongodb.model.Person;
import tutorial.spring.data.mongodb.repository.PersonRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PersonController {

  @Autowired
  PersonRepository personRepository;

  @GetMapping("/persons")
  public ResponseEntity<List<Person>> getAllTutorials() {
    try {
      List<Person> people = new ArrayList<Person>();
      personRepository.findAll().forEach(people::add);

      if (people.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(people, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/persons/{id}")
  public ResponseEntity<Person> getPersonById(@PathVariable("id") String id) {
    Optional<Person> personData = personRepository.findById(id);

    if (personData.isPresent()) {
      return new ResponseEntity<>(personData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/persons")
  public ResponseEntity<String> createPerson(@RequestBody Person person) {
    try {
      Person _person = personRepository.save(new Person(person.getName(), person.getFamily()));
      return new ResponseEntity<>(_person.getId(), HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/persons/{id}")
  public ResponseEntity<Person> updatePerson(@PathVariable("id") String id, @RequestBody Person person) {
    Optional<Person> personData = personRepository.findById(id);

    if (personData.isPresent()) {
      Person _person = personData.get();
      _person.setName(person.getName());
      _person.setFamily(person.getFamily());
      return new ResponseEntity<>(personRepository.save(_person), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/persons/{id}")
  public ResponseEntity<HttpStatus> deletePerson(@PathVariable("id") String id) {
    try {
      personRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @DeleteMapping("/persons")
  public ResponseEntity<HttpStatus> deleteAllPersons() {
    try {
      personRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @RequestMapping(path = "/create-file", method = RequestMethod.POST)
  public ResponseEntity<Resource> createFile(@RequestBody FileDto fileDto) throws IOException {

    if (fileDto == null || fileDto.getName() == null || fileDto.getName().trim().isEmpty()) return ResponseEntity.status(400).build();

    Random r = new Random();
    StringBuffer buff = new StringBuffer();
    for (int i = 0; i < 1000; i++) {
      char c = (char)(r.nextInt(26) + 'a');
      buff.append(c);
    }
    File outputFile = new File("files/", fileDto.getName());
    try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
      outputStream.write(buff.toString().getBytes(StandardCharsets.US_ASCII));
    } catch (IOException e) {
      return ResponseEntity.status(500).build();
    }

    try {
      String checksum = checksum(outputFile);
      File checksumFile = new File("files/", outputFile.getName() + ".sha512");
      try (FileOutputStream outputStream = new FileOutputStream(checksumFile)) {
        outputStream.write(checksum.getBytes(StandardCharsets.US_ASCII));
      }

    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
    return ResponseEntity.ok().build();
  }


  @RequestMapping(path = "/download-file/{name}", method = RequestMethod.GET)
  public ResponseEntity<Resource> download(@PathVariable("name") String name) throws IOException {
    if (name == null || name.isEmpty()) return ResponseEntity.status(400).build();
    File file = new File("files/", name);
    if (!file.exists()) return ResponseEntity.status(404).build();
    return downloadFile(file);
  }

  private static ResponseEntity<Resource> downloadFile(File file) throws IOException {
    Path path = Paths.get(file.getAbsolutePath());
    ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

    HttpHeaders header = new HttpHeaders();
    header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
    header.add("Cache-Control", "no-cache, no-store, must-revalidate");
    header.add("Pragma", "no-cache");
    header.add("Expires", "0");

    return ResponseEntity.ok()
            .headers(header)
            .contentLength(file.length())
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .body(resource);
  }

  /*
  echo "$(cat filename.sha512) filename" | sha512sum --check
   */
  private String checksum(File file) throws NoSuchAlgorithmException, IOException {
    return com.google.common.io.Files.asByteSource(file).hash(Hashing.sha512()).toString();
  }
}
