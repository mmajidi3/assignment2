package tutorial.spring.data.mongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "persons")
public class Person {
  @Id
  private String id;

  private String name;
  private String family;

  public Person() {

  }

  public Person(String name, String family) {
    this.name = name;
    this.family = family;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFamily() {
    return family;
  }

  public void setFamily(String family) {
    this.family = family;
  }

  public String toString() {
    return "Person [id=" + id + ", name=" + name + ", family=" + family + "]";
  }
}
