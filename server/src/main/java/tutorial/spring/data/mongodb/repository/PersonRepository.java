package tutorial.spring.data.mongodb.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import tutorial.spring.data.mongodb.model.Person;

public interface PersonRepository extends MongoRepository<Person, String> {

}
