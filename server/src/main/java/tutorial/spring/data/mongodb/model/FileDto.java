package tutorial.spring.data.mongodb.model;

public class FileDto {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
