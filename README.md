## CS 378: Assignment 2 - Dockers and Containers

## Build requirements

java 1.8 
maven +3.6

## Environment variables

update server ehternet IP address in fileclient.sh. do not use localhost.

## How to start

use fileserver.sh to build and create the server containser.

make sure /serverdata and /clientdata is accessible. 

then run the fileclient.sh

the following actions is done by client:

- client code creates a new person and saves the generated id

- queries the current list of users.

- queries the person with id

- deletes the person

- creates a random text file at server

- downloads the random file and its sha512 hash file

- prints the content of the random file and its hash file

- checks the file integrity and writes the result in output.



